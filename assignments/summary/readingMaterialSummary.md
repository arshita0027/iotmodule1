### **IoT Basics**
#### 1.Industrial Revolution
 - Industry 1.0   
   1. Started with the use of steam power and mechanization of products.  
   2. Increased human productivity and production of goods.  
   3. Movement of goods and humans to greater distances within fewer hours.
 - Industry 2.0   
   1. Began with discovery of electricity and assembly line production.  
   2. Enabled mass production of goods.  
   3. Production in partial steps on conveyor belt improved rate of production at lower costs.
 - Industry 3.0   
   1. Started with use of computers and memory-programmable controllers.  
   2. Introduction to Information Technology and renewable energy.  
   3. Enabled partial automation of production process.
 - Industry 4.0   
   1. Biggest credit to application of Internet to industrial processes (IoT).  
   2. Era of Cyber-physical systems and Cloud computing.  
   3. Smart machines can exchange information, trigger action and control each other without human intervention.  

#### 2.Industry 3.0

Sensors would send the data from factory points and to PLCs which collect it and send to SCADA for storing.   
Protocols are used by sensors to send data to PLCs   

*Indusrty 3.0 protocols-*   
 - Modbus- Modbus TCP and Modbus RTU are widely used in IoT devices.
 - CANopen- useful in embedded systems used for automation
 - Ethercat- Ethernet based field bus suitable for real-time computation
 - Profinet- Industrial ethernet
Industry 3.0 architecture:

![](https://40uu5c99f3a2ja7s7miveqgqu-wpengine.netdna-ssl.com/wp-content/uploads/2017/04/Automation-pyramid-by-invilution-source-and-credits-invilution.jpg)


#### 3.Industry 4.0 

 - Industry 4.0 is Industry 3.0 devices connected to the internet. (Internet of things)  
 - Industry 4.0 is an upgrade to industry 3.0 improving in connectivity of devices and less or no human intervention.
 - *I4 Communication protocols-*   
   - MQTT(Message Query Telemetry Transport)
   - AMQP ( Advanced Messaging Queuing Protocol)
   - HTTP( HyperText Transfer Porotcol )
   - CoAP( Constrained Application Protocol)
   - REST ( RepresentationalState Transfer) API- specially used in web services
   - Websockets- This protocol provides dual-way communicatin over single TCP connection
 - *Upgrading industry 3.0 devices to industry 4.0:*
   - Get data from factory using Industry 3.0 devices and send it to cloud using Industry 4.0 devices.This way upgrading to industry 4.0 without much changes
   -  Most important step in this process is conversion of Industry 3.0 protocols into Industry 4.0 protocols.
   - This is done using embedded boards like Raspberry Pi, Tinkerboard, etc. and connecting them over Bluetooth, Ethernet, GSM, Wifi whichever is suitable.
 - *IoT tools:*
   - Platforms- AWS IoT, Google IoT, Azure IoT, Thingsboard
   - Dashboard- Grafana, Thingsboard
   - TSDB tools- Prometheus, InfluxDB
   - Alert- Zaiper, Twilio
- *Cloud features ( taking example of Altair smart core IoT platform):*  
   - Devices- The "things" in an IoT network.They are core of projects.   
   - Streams-Information or data streams in IoT network that keep projects alive.   
   - Listeners-These are executed when an related event occurs for the entity listened.   
   - Alarms- Notification or signals that are generated when device changes its status.   
![ ](https://cdn-a.william-reed.com/var/wrbm_gb_food_pharma/storage/images/5/3/7/1/5741735-7-eng-GB/Smart-food-factories-move-even-closer_wrbm_large.jpg)


